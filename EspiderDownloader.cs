﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Windows.Forms;

namespace EspiderDownloader
{
    class TimeoutWebClient : WebClient
    {
        int timeout = 0;

        public TimeoutWebClient(int _timeout)
        {
            timeout = _timeout;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);
            request.Timeout = timeout;

            return request;
        }
    }

    public partial class EspiderDownloader : Form
    {
        public EspiderDownloader()
        {
            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            String commandKey = "espider\\shell\\open\\command";

            RegistryKey command = Registry.ClassesRoot.CreateSubKey(commandKey, RegistryKeyPermissionCheck.ReadWriteSubTree);
            //command.DeleteValue("(기본값)");
            String exePath = Assembly.GetEntryAssembly().Location;
            String value = String.Format("\"{0}\" \"%1\"", exePath);
            command.SetValue("", value, RegistryValueKind.String);

            RegistryKey espider = Registry.ClassesRoot.OpenSubKey("espider", true);
            espider.SetValue("URL protocol", "", RegistryValueKind.String);

            MessageBox.Show("등록되었습니다.");
            Application.Exit();
        }

        private void EspiderDownloader_Load(object sender, EventArgs e)
        {
            string[] args = Environment.GetCommandLineArgs();
            //args = new String[] { "aaa", "espider://10;G/" };
            if(args.Length > 1)
            {
                string param = args[1];
                if ("DEV".Equals(param))  //등록 모드.
                {
                    this.pnlRegister.Show(); 
                    this.pnlDownload.Hide();
                    this.pnlRegister.Dock = DockStyle.Fill;
                    this.Height = 100;
                    this.Width = 440;
                }
                else if (param.IndexOf("espider://") != 0 || param.IndexOf(";") == -1)  //espider://로 시작하는지랑 중간에 세미콜론(;)이 있는지  확인.
                {
                    MessageBox.Show("파라미터 정보가 잘못되었습니다.");
                    Application.Exit();
                }
                else  //다운로드 모드. 
                {
                    this.pnlRegister.Hide();
                    this.pnlDownload.Show();
                    this.pnlDownload.Dock = DockStyle.Fill;
                    this.Height = 100;
                    this.Width = 440;
                    this.timerDownload.Interval = 100;  //0.1초후 다운로드 시작.
                    this.timerDownload.Enabled = true;

                    this.lblStatus.Text = "파일 목록 조회중...";
                } //End-of-if (espiderRegistry == null || args.Length == 1)
            }
            else
            {
                MessageBox.Show("파라미터 정보가 없습니다.");
                Application.Exit();
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.timerDownload.Enabled = false;

            String execPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            String logPath = String.Format("{0}\\espiderDownloader.log", execPath);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(DateTime.Now.ToString());

            string[] args = Environment.GetCommandLineArgs(); 
            //args = new String[] { "aaa", "espider://10;G/" };

            bool isSaveStep = false;

            try
            {
                String info = args[1];
                sb.AppendLine(String.Format("Argument={0}", info));

                info = info.Replace("espider://", "");

                string[] arr = info.Split(';');
                if(arr.Length != 3)
                {
                    MessageBox.Show("파라미터 정보가 잘못되었습니다.");
                    Application.Exit();
                }
                String espiderUrl = arr[0];
                String key = arr[1];       //문서KEY
                String drive = arr[2].Replace(":", "").Replace("/", "");     //저장할 USB 드라이브
                String downloadDir = String.Format("{0}\\download", execPath);

                sb.AppendLine(String.Format("espiderUrl={0}", espiderUrl));
                sb.AppendLine(String.Format("key={0}", key));
                sb.AppendLine(String.Format("drive={0}", drive));
                sb.AppendLine(String.Format("downloadDir={0}", downloadDir));

                /*----------------------------------------------------
                * 다운로드할 파일 목록 JSON 받아서 파싱하기.
                *----------------------------------------------------*/
                String url = String.Format("{0}/api/takeout/files/{1}", espiderUrl, key);
                sb.AppendLine(String.Format("eSpider.FileList.URL={0}", url));

                TimeoutWebClient client = new TimeoutWebClient(180000);
                client.Encoding = Encoding.UTF8;
                string result = client.DownloadString(url);
                sb.AppendLine(String.Format("...Result={0}", result));

                //파일 목록 파싱.
                List<FileInfoModel> list = JsonConvert.DeserializeObject<List<FileInfoModel>>(result);
                sb.AppendLine(String.Format("...File.List.Count={0}", list.Count()));

                //파일 전체 길이 계산.
                long totalLength = 0;
                foreach (FileInfoModel item in list)
                {
                    if (!item.compactFileSize.HasValue) item.compactFileSize = 1;
                    totalLength += (int)item.compactFileSize;
                }

                /*----------------------------------------------------
                    * 파일 다운로드 및 SafePC 호출.
                    *----------------------------------------------------*/
                long downloadLength = 0;

                //임시 폴더 없으면 생성.
                if (!Directory.Exists(downloadDir))
                {
                    Directory.CreateDirectory(downloadDir);
                }

                List<String> localPathList = new List<String>();

                sb.AppendLine("Download start...");

                //파일 다운로드.
                foreach (FileInfoModel item in list)
                {
                    String fileName = String.Format("{0}{1}", item.originalFileName, item.fileExtension);

                    var downClient = new WebClient();
                    downClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                    //MessageBox.Show(item.url);
                    String downloadUrl = String.Format("{0}{1}", espiderUrl, item.url);
                    sb.AppendLine(String.Format("...Download.url={0}", downloadUrl));

                    this.lblStatus.Text = String.Format("{0} 다운로드 중...", fileName);

                    //MessageBox.Show(downloadUrl);
                    String savePath = String.Format("{0}\\{1}", downloadDir, fileName);
                    //Console.WriteLine(downloadUrl);
                    //Console.WriteLine(savePath);
                    Uri uri = new Uri(downloadUrl);
                    downClient.DownloadFile(uri, savePath);
                    sb.AppendLine(String.Format("...File.saved={0}", savePath));

                    localPathList.Add(savePath);

                    downloadLength += (int)item.compactFileSize;
                    this.progressBar.Value = Convert.ToInt32((downloadLength / totalLength) * 100);
                }

                sb.AppendLine("SafePC call start...");

                //SafePC 호출.
                isSaveStep = true;
                foreach (String srcPath in localPathList)
                {
                    String fileName = Path.GetFileName(srcPath);
                    this.lblStatus.Text = String.Format("{0} 저장 중...", fileName);

                    String tgtPath = String.Format("{0}:\\{1}", drive, fileName);
                    String param = String.Format("{0}\t{1}\tfalse", srcPath, tgtPath);
                    String safeUrl = String.Format("https://127.0.0.1:18090/SafeExternalInterWork?method=CopyFileEx&params={0}", Uri.EscapeUriString(param));
                    sb.AppendLine(String.Format("...SafePC.URL={0}", safeUrl));
                    
                    TimeoutWebClient safeClient = new TimeoutWebClient(180000);
                    safeClient.Encoding = Encoding.UTF8;
                    string response = client.DownloadString(safeUrl);
                    sb.AppendLine(String.Format("...SafePC.response={0}", response));

                    //처리 목록 파싱.
                    ResultModel safeResult = JsonConvert.DeserializeObject<ResultModel>(response);
                    String errMessage = "성공";
                    switch (safeResult.code)
                    {
                        case 1:
                            errMessage = "잘못된 파일 경로(원본 or 목적지 경로)";
                            break;
                        case 2:
                            errMessage = "파일이 이미 존재함(목적지경로)";
                            break;
                        case 3:
                            errMessage = "복사할 공간이부족함";
                            break;
                        case 4:
                            errMessage = "기타 예기치 못한 오류";
                            break;
                        case 5:
                            errMessage = "대상 경로로 복사 권한 없음";
                            break;
                        default:    //0=성공.
                            break;
                    }

                    if (safeResult.code != 0)
                    {
                        MessageBox.Show(errMessage);
                        sb.AppendLine(String.Format("...SafePC.error : Code={0}, Message={1}", safeResult.code, errMessage));
                        File.AppendAllText(logPath, sb.ToString(), Encoding.UTF8);
                        Application.Exit();
                    }
                    else
                    {
                        //MessageBox.Show(errMessage);
                        sb.AppendLine(String.Format("...SafePC.success : Code={0}, Message={1}", safeResult.code, errMessage));
                        File.AppendAllText(logPath, sb.ToString(), Encoding.UTF8);
                    }

                }

                //File.AppendAllText(logPath, sb.ToString(), Encoding.UTF8);
                //File.WriteAllText(logPath, sb.ToString(), Encoding.UTF8);
                MessageBox.Show(" 저장되었습니다.");
                Application.Exit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                sb.AppendLine(ex.ToString());
                File.AppendAllText(logPath, sb.ToString(), Encoding.UTF8);
                //File.WriteAllText(logPath, sb.ToString(), Encoding.UTF8);

                String errMessage = (isSaveStep) ? "파일 저장시 오류가 발생하였습니다." : "파일 다운로드 중 오류가 발생하였습니다.";
                MessageBox.Show(errMessage);
                Application.Exit();
            }

        }   //End-of-timer1_Tick

    }

}
