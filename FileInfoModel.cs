﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EspiderDownloader
{
    class FileInfoModel
    {
        public string url { get; set; }
        public string originalFileName { get; set; }
        public string fileExtension { get; set; }
        public int? compactFileSize { get; set; }

    }
}
